FROM python:3.9
ENV HELM_VERSION=3.9.0
ENV HELMFILE_VERSION=0.145.2
ENV TERRAFORM_VERSION=1.2.4

RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list &&\
      curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - &&\
      apt-get update &&\
      apt-get install -y \
      curl \
      docker.io \
      git \
      google-cloud-cli \
      jq \
      less \
      libatspi2.0-0 \
      libgtk-3-0 \
      libnotify4 \
      libnss3 \
      libsecret-1-0 \
      libxtst6 \
      postgresql-client \
      xdg-utils &&\
      curl -so- https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz | tar xzf - &&\
      install linux-amd64/helm /usr/local/bin &&\
      rm -rf linux-amd64 &&\
      curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" &&\
      install kubectl /usr/local/bin && rm -rf kubectl &&\
      helm plugin install https://github.com/chartmuseum/helm-push.git &&\
      curl -sLo /tmp/helmfile.tar.gz https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_${HELMFILE_VERSION}_linux_amd64.tar.gz &&\
      tar -C /usr/local/bin -xzf /tmp/helmfile.tar.gz helmfile &&\
      chmod +x /usr/local/bin/helmfile &&\
      rm -rf /tmp/helmfile.tar.gz &&\
      curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" &&\
      unzip awscliv2.zip &&\
      ./aws/install -i /usr/local/aws-cli -b /usr/local/bin &&\
      python -m venv /venv &&\
      . /venv/bin/activate &&\
      pip install -U pip &&\
      pip install ansible hcloud &&\
      curl -s https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip > terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
      unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin && \
      rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip &&\
      curl -Lo exoscale.tar.gz https://github.com/exoscale/cli/releases/download/v1.59.0/exoscale-cli_1.59.0_linux_amd64.tar.gz &&\
      tar -xzf exoscale.tar.gz -C /usr/local/bin exo &&\
      rm -f exoscale.tar.gz &&\
      ln -sf /usr/bin/python /usr/bin/python3 &&\
      curl -sL https://github.com/mikefarah/yq/releases/download/3.3.2/yq_linux_amd64 -o /usr/local/bin/yq &&\
      chmod +x /usr/local/bin/yq &&\
      mkdir -p "$HOME"/.helm/plugins &&\
      helm plugin install https://github.com/hayorov/helm-gcs

RUN apt-get install php php-xml php-mbstring mariadb-client -y &&\
      curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer